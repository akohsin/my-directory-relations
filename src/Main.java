import java.io.File;

public class Main {
    public static void main(String[] args) {
        String path = "C:\\Users\\Arkadiusz\\IdeaProjects\\folder";
        File f = new File(path);
        String tab = "   ";
        function2(tab, f);
    }

    public static void function2(String tab, File file) {

        File[] fArray = file.listFiles();
        for (File fi : fArray) {
            String tab2 = tab + "   ";
            System.out.println(tab + fi.getName());
            if (fi.isDirectory()) {
                function2(tab2, fi);
                tab2 = tab;
            }
        }
    }
}
